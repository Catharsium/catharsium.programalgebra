﻿namespace Catharsium.ProgramAlgebra.Core.Entities.Instructions.PGLB
{
    public class BackwardJump : Instruction
    {
        public int Instructions { get; set; }
    }
}