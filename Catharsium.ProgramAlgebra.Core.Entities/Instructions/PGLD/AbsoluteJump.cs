﻿namespace Catharsium.ProgramAlgebra.Core.Entities.Instructions.PGLD
{
    public class AbsoluteJump : Instruction
    {
        public int Index { get; set; }
    }
}