﻿namespace Catharsium.ProgramAlgebra.Core.Entities.Instructions.PGLDg
{
    public class AbsoluteGotoInstruction : Instruction
    {
        public string Label { get; set; }
    }
}