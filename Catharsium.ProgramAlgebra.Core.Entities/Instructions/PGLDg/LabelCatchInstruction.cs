﻿namespace Catharsium.ProgramAlgebra.Core.Entities.Instructions.PGLDg
{
    public class LabelCatchInstruction : Instruction
    {
        public string Label { get; set; }
    }
}