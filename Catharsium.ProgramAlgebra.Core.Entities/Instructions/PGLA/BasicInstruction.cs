﻿namespace Catharsium.ProgramAlgebra.Core.Entities.Instructions.PGLA
{
    public class BasicInstruction : Instruction
    {
        public string Label { get; set; }
    }
}