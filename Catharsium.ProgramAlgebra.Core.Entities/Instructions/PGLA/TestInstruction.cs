﻿namespace Catharsium.ProgramAlgebra.Core.Entities.Instructions.PGLA
{
    public class TestInstruction : Instruction
    {
        public bool Positive { get; set; }
    }
}