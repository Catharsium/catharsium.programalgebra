﻿using Catharsium.Util.Configuration.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Catharsium.ProgramAlgebra.Core.Logic.Configuration
{
    public static class LogicCoreProgramAlgebraRegistration
    {
        public static IServiceCollection AddIoUtilities(this IServiceCollection services, IConfiguration config)
        {
            var configuration = config.Load<LogicCoreProgramAlgebraConfiguration>();
            
            return services;
        }
    }
}